package variable

import (
	"fmt"
	"unsafe"
)

func Variable_one()  {
	//声明变量的一般形式是使用 var 关键字： var identifier type
	var name string = "zhangsan"
	// 可以一次声明多个变量：var identifier1, identifier2 type
	var address1,address2 string = "shanghai","shenzhen"

	println(name)
	println(address1,address2)

	//变量声明
	//第一种，指定变量类型，如果没有初始化，则变量默认为零值。
	// 声明一个变量并初始化
	var a = "baidu"
	println(a)
	// 声明一个变量未初始化  默认值： false
	var flag bool
	println(flag)
	// 声明一个变量未初始化  默认值： 0
	var d int
	println(d)
	// 第二种，根据值自行判定变量类型。
	var v_name = name
	println(v_name)

	// 第三种： 简短声明并赋值  不需要 var
	str := "to be or not to be,this is a question!"
	println(str)

	//nil是一个预先声明的标识符，表示指针、通道、函数、接口、映射或切片类型。
	//var a *int
	//var a []int
	//var a map[string] int
	//var a chan int
	//var a func(string) int
	//var a error // error 是接口
}

func Variable_two()  {
	var a,b int = 1,2
	println(a,b)

	var c,d = 1,"hello"
	println(c,d)

	//这种不带声明格式的只能在函数体中出现
	e,f := 1,"hello,go!"
	println(e,f)
}

func Variable_three()  {
	const WIDTH = 10.2
	const HEIGHT = 6

	const a,b,c = 1,2,"admin"
	// 常量还可以 做 枚举
	const (
		Unknow = 0
		Female = 1
		Male = 2
	)
	// iota，特殊常量，可以认为是一个可以被编译器修改的常量。iota 在 const关键字出现时将被重置为 0
	//(const 内部的第一行之前)，const 中每新增一行常量声明将使 iota 计数一次(iota 可理解为 const 语句块中的行索引)。
	const (
		x = iota
		y = iota
		z = iota
	)

	var area = WIDTH * HEIGHT
	println("面积为： ",area)
	println(a,b,c)
	// unsafe.Sizeof() 字符串类型在 go 里是个结构, 包含指向底层数组的指针和长度,这两部分每部分都是 8 个字节，所以字符串类型大小为 16 个字节。
	println("len: ",len(c),"size: ",unsafe.Sizeof(c))
	println(Female)
	// 第一个 iota 等于 0，每当 iota 在新的一行被使用时，它的值都会自动加 1；所以 a=0, b=1, c=2 可以简写为如下形式：
	println("x: ",x ,"y: ",y,"z: ",z)

	const (
		j = iota   //0
		k          //1
		l          //2
		d = "ha"   //独立值，iota += 1
		e          //"ha"   iota += 1
		f = 100    //iota +=1
		g          //100  iota +=1
		h = iota   //7,恢复计数
		i          //8
	)
	fmt.Println(j,k,l,d,e,f,g,h,i)
}


