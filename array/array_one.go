package array

import "fmt"

func Array_one() {
	// 初始化 數組
	var balance = [5]float32{100, 12, 19, 39, 99}

	// 如果数组长度不确定，可以使用 ... 代替数组的长度，编译器会根据元素个数自行推断数组的长度：
	var balance2 = [...]float32{1, 2, 1}

	//  将索引为 1 和 3 的元素初始化
	balance3 := [5]float32{1: 2.0, 3: 7.0}

	// 数组元素可以通过索引（位置）来读取
	println(balance[3])
	println(balance2[1])
	println(balance3[2])

	//  输出每个数组元素的值
	for i := 0; i < len(balance); i++ {
		fmt.Printf("Element[%d] = %d\\n", i, balance[i])
	}

	for i := 0; i < len(balance2); i++ {
		fmt.Printf("index: %d, value: %d", i, balance2[i])
	}
	println()
}

// 二维数组
func Array_two() {
	// Step 1: 创建数组
	values := [][]int{}

	// Step 2: 使用 append() 函数向空的二维数组添加两行一维数组
	row1 := []int{1, 2, 3}
	row2 := []int{6, 7, 8}
	values = append(values, row1)
	values = append(values, row2)

	// Step 3: 显示两行数据
	println("row1: ")
	fmt.Println(values[0])
	println("row2: ")
	fmt.Println(values[1])

	// Step 4: 访问第一个元素
	println(values[0][0])

	// 初始化二维数组
	// 多维数组可通过大括号来初始值。以下实例为一个 3 行 4 列的二维数组：
	a := [3][4]int{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 10, 11, 12},
	}
	fmt.Println(a[0])

	// 创建二维数组
	sites := [2][2]string{}
	// 往数组添加元素
	// 向二维数组添加元素
	sites[0][0] = "Google"
	sites[0][1] = "Runoob"
	sites[1][0] = "Taobao"
	sites[1][1] = "Weibo"
	fmt.Println(sites)

	// 访问数组元素
	val := sites[1][1]
	fmt.Println(val)

	/* 输出数组元素 */
	var i, j int
	for i = 0; i < 2; i++ {
		for j = 0; j < 2; j++ {
			fmt.Print("sites[%d][%d] = %d\n", i, j, sites[i][j])
		}
	}
	println()

	// 创建空的二维数组
	animals := [][]string{}

	// 创建三一维数组，各数组长度不同
	row4 := []string{"fish", "shark", "eel"}
	row5 := []string{"bird"}
	row6 := []string{"lizard", "salamander"}

	animals = append(animals, row4)
	animals = append(animals, row5)
	animals = append(animals, row6)
	// 循环输出
	for i3 := range animals {
		fmt.Println(animals[i3])
	}
}
