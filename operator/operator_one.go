package operator

import "fmt"

// 运算符





func Operator_one()  {
	var a int = 10
	var b int = 2
	var c int
	var d int = 13

	//  算术运算符
	c = a + b
	fmt.Printf(" 第一行 - c 的值为 %d",c)
	c = a - b
	fmt.Printf(" 第二行 - c 的值为 %d",c)
	c = a * b
	fmt.Printf(" 第三行 - c 的值为 %d",c)
	c = a / b
	fmt.Printf(" 第四行 - c 的值为 %d",c)
	c = a % b
	fmt.Printf(" 第五行 - c 的值为 %d",c)
	a ++
	fmt.Printf(" 第六行 - c 的值为 %d",a)
	a=21   // 为了方便测试，a 这里重新赋值为 21
	a --
	fmt.Printf(" 第七行 - c 的值为 %d",a)

	// 关系运算符
	println("res: ",b == d)
	println("res: ",b != d)
	println("res: ",b > d)
	println("res: ",b < d)
	println("res: ",b >= d)
	println("res: ",b <= d)

	// 逻辑运算符
	println("res: ", a > b && c > b)
	println("res: ", a > b || c > d)
	println("res: ",!(a >b))

	// 位运算 下表列出了位运算符 &, |, 和 ^ 的计算：
	// & 按位与 都为1  				结果为1
	// | 按位或 有一个结果为1   		结果为1
	// ^ 按位异 当两对应的二进位相异时	结果为1
	// <<	左移运算符"<<"是双目运算符。左移n位就是乘以2的n次方
	// >>	右移运算符">>"是双目运算符。右移n位就是除以2的n次方。
	//	p	q	p & q	p | q	p ^ q
	//	0	0	0	0	0
	//	0	1	0	1	1
	//	1	1	1	1	0
	//	1	0	0	1	1
	var A int = 60
	var B int = 10

	println("res: ",A & B)
	println("res: ",A | B)
	println("res: ",A ^ B)
	println("res:(60 * 4) ",(A << 2))
	fmt.Printf("res:(60/4) ",(A >> 2))

	// 赋值运算符
	//=	简单的赋值运算符，将一个表达式的值赋给一个左值	C = A + B 将 A + B 表达式结果赋值给 C
	//+=	相加后再赋值	C += A 等于 C = C + A
	//-=	相减后再赋值	C -= A 等于 C = C - A
	//*=	相乘后再赋值	C *= A 等于 C = C * A
	///=	相除后再赋值	C /= A 等于 C = C / A
	//%=	求余后再赋值	C %= A 等于 C = C % A
	//<<=	左移后赋值	C <<= 2 等于 C = C << 2
	//>>=	右移后赋值	C >>= 2 等于 C = C >> 2
	//&=	按位与后赋值	C &= 2 等于 C = C & 2
	//^=	按位异或后赋值	C ^= 2 等于 C = C ^ 2
	//|=	按位或后赋值	C |= 2 等于 C = C | 2

	c =  a
	fmt.Printf("第 1 行 - =  运算符实例，c 值为 = %d\n", c )

	c +=  a
	fmt.Printf("第 2 行 - += 运算符实例，c 值为 = %d\n", c )

	c -=  a
	fmt.Printf("第 3 行 - -= 运算符实例，c 值为 = %d\n", c )

	c *=  a
	fmt.Printf("第 4 行 - *= 运算符实例，c 值为 = %d\n", c )

	c /=  a
	fmt.Printf("第 5 行 - /= 运算符实例，c 值为 = %d\n", c )

	c  = 200;

	c <<=  2
	fmt.Printf("第 6行  - <<= 运算符实例，c 值为 = %d\n", c )

	c >>=  2
	fmt.Printf("第 7 行 - >>= 运算符实例，c 值为 = %d\n", c )

	c &=  2
	fmt.Printf("第 8 行 - &= 运算符实例，c 值为 = %d\n", c )

	c ^=  2
	fmt.Printf("第 9 行 - ^= 运算符实例，c 值为 = %d\n", c )

	c |=  2
	fmt.Printf("第 10 行 - |= 运算符实例，c 值为 = %d\n", c )

	//其他运算符
	//&	返回变量存储地址	&a; 将给出变量的实际地址。
	//*	指针变量。	*a; 是一个指针变量
	var ptr *int

	/* 运算符实例 */
	fmt.Printf("第 1 行 - a 变量类型为 = %T\n", a );
	fmt.Printf("第 2 行 - b 变量类型为 = %T\n", b );
	fmt.Printf("第 3 行 - c 变量类型为 = %T\n", c );

	/*  & 和 * 运算符实例 */

	ptr = &a     /* 'ptr' 包含了 'a' 变量的地址 */
	fmt.Printf("a 的值为  %d\n", a);
	fmt.Printf("*ptr 为 %d\n", *ptr);

	//运算符优先级
	//有些运算符拥有较高的优先级，二元运算符的运算方向均是从左至右。下表列出了所有运算符以及它们的优先级，由上至下代表优先级由高到低：
	//
	//优先级	运算符
	//5	* / % << >> & &^
	//4	+ - | ^
	//3	== != < <= > >=
	//2	&&
	//1	||


}