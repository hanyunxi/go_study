package languageFunc

/* 函数返回两个数的最大值 */
func Max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
