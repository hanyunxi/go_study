package dataType

import (
	"strings"
)

/* 声明全局变量 */
var IsActive bool                   // 全局变量声明
var enabled, disabled = true, false // 忽略类型的声明

func DataType() {
	/**声明局部变量*/
	var a, b = 1.5, 2
	println(a, b)
	var available bool // 一般声明
	valid := false     // 简短声明
	available = true   // 赋值操作
	println(available, valid)
}

func Str() {
	//:= 是声明并赋值，并且系统自动推断类型，不需要var关键字
	str := "这里是 www\n.runoob\n.com"
	println("-------- 原字符串 ----------")
	println(str)
	// 去除空格
	str = strings.Replace(str, " ", "", -1)
	// 去除换行符
	str = strings.Replace(str, "\n", "", -1)
	println("-------- 去除空格与换行后 ----------")
	println(str)
}
