package condition

// 条件语句
func Condition_if() {
	var a int = 20
	if a > 20 {
		println("大于20")
	} else if a > 10 {
		println("大于10")
	} else {
		println("小于10")
	}
}

// 循环语句
func Condition_loop_for() {
	// or 循环	重复执行语句块
	//循环嵌套	在 for 循环中嵌套一个或多个 for 循环
	//for true {
	//	i := 1
	//	if i == 3 {
	//		break
	//	}
	//	println("hello, go!")
	//	i++
	//}

	/*	break 语句	经常用于中断当前 for 循环或跳出 switch 语句
		continue 语句	跳过当前循环的剩余语句，然后继续进行下一轮循环。
		goto 语句	将控制转移到被标记的语句。*/

	// 不使用标记		break
	for i := 0; i < 10; i++ {
		println(i)
		if i == 3 {
			break
		}
	}
	// 使用标记
re:
	for i := 1; i < 10; i++ {
		println(i)
		if i == 3 {
			break re
		}
	}
	// continue
	var a int = 10
	for a < 20 {
		if a == 15 {
			a = a + 1
			continue
		}
		println("a的值： ", a)
		a++
	}

	// goto	在变量 a 等于 15 的时候跳过本次循环并回到循环的开始语句 LOOP 处：
	b := 10
r:
	for b < 20 {
		if b == 15 {
			b = b + 1
			goto r
		}
		println("b的值： ", b)
		b++
	}
}
