package structure

type Books struct {
	title   string
	author  string
	subject string
	bookid  int
}
