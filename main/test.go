package main

import "go_study/dataType"
import "go_study/variable"
import "go_study/operator"
import "go_study/condition"
import "go_study/languageFunc"
import "go_study/array"
import "go_study/pointer"

func main() {
	dataType.DataType()
	println(dataType.IsActive)
	dataType.Str()

	variable.Variable_one()
	variable.Variable_two()
	variable.Variable_three()

	operator.Operator_one()

	// 条件语句
	condition.Condition_if()
	// 循环语句
	condition.Condition_loop_for()

	// 语言函数
	println("比较最大值(a,b)： ", languageFunc.Max(5, 10))

	array.Array_one()

	array.Array_two()

	pointer.Pointer_one()
}
