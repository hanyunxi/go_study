package main

import "fmt"
import "go_study/method"



//常量定义
const PI = 3.14
// 全局变量的声和赋值
var age int = 10
var name string = "zhangsan"
var address string = "shanghai"

// 字符串格式化
func strFormat()  {
	// %d 表示整型数字，%s 表示字符串
	var stockcode =123
	var enddate="2020-12-31"
	var url="Code=%d&endDate=%s"
	var target_url = fmt.Sprintf(url,stockcode,enddate)
	println(target_url)
}


// 由main函数作为程序入口点启动
// Go 程序是通过 package 来组织的。
//只有 package 名称为 main 的源码文件可以包含 main 函数。
//一个可执行程序有且仅有一个 main 包。
//通过 import 关键字来导入其他非 main 包。
//可以通过 import 关键字单个导入:
func main()  {
	strFormat()
	var res = myMath.Add(1,1)
	println(res)
}