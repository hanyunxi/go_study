package main

import "fmt"

// Go 语言切片是对数组的抽象。
// 定义切片
//你可以声明一个未指定大小的数组来定义切片：

func main() {
	// var slice1 []type = make([]type, len)
	//也可以简写为
	//slice1 := make([]type, len)

	// 切片 初始化
	s := []int{1, 2, 3, 4, 5, 6, 7}

	//	初始化切片 s，是数组 arr 的引用。
	//	s := arr[startIndex:endIndex]
	s = s[0:3]
	fmt.Print(s)

	// len() 和 cap() 函数
	var numbers []int

	printSlice(numbers)

	if numbers == nil {
		fmt.Printf("切片是空的")
	}

	/* 创建切片 */
	numbers = []int{0, 1, 2, 3, 4, 5, 6, 7, 8}
	printSlice(numbers)

	/* 打印原始切片 */
	fmt.Println("numbers ==", numbers)

	/* 打印子切片从索引1(包含) 到索引4(不包含)*/
	fmt.Println("numbers[1:4] ==", numbers[1:4])

	/* 允许追加空切片 */
	numbers = append(numbers, 0)
	printSlice(numbers)

	/* 向切片添加一个元素 */
	numbers = append(numbers, 1)
	printSlice(numbers)

	/* 同时添加多个元素 */
	numbers = append(numbers, 2, 3, 4)
	printSlice(numbers)

	/* 创建切片 numbers1 是之前切片的两倍容量*/
	numbers1 := make([]int, len(numbers), (cap(numbers))*2)

	/* 拷贝 numbers 的内容到 numbers1 */
	copy(numbers1, numbers)
	printSlice(numbers1)
}

func printSlice(x []int) {
	fmt.Printf("len=%d cap=%d slice=%v\n", len(x), cap(x), x)
}
