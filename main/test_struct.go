package main

import "fmt"

// 定义结构体
type Books struct {
	title   string
	author  string
	subject string
	bookid  int
}

func main() {
	//println(structure.Books{"GO从入门到放弃", "张三", "Go", 1})
	// 创建一个新的结构体
	fmt.Println(Books{"Go 语言", "www.runoob.com", "Go 语言教程", 6495407})

	// 也可以 key -> value 格式
	fmt.Println(Books{"Go 语言", "www.runoob.com", "Go 语言教程", 6495407})

	// 忽略的字段为 0 或 空
	fmt.Println(Books{title: "Go 语言", author: "www.runoob.com"})

	// 访问结构体成员
	var books1 Books
	var books2 Books

	books1.title = "GO 语言 从入门到放弃"
	books1.author = "张三"
	books1.subject = "Go 语言教程"
	books1.bookid = 10560

	books2.title = "Python 从入门到放弃"
	books2.author = "张三"
	books2.subject = "Python 语言教程"
	books2.bookid = 10560

	/* 打印 Book1 信息 */
	println("books1 title :", books1.title)
	println("books1 author :", books1.author)
	println("books1 subject :", books1.subject)
	println("books1 bookid :", books1.bookid)

	/*打印 Book2 信息*/
	println("books2 title :", books2.title)
	println("books2 author :", books2.author)
	println("books2 subject :", books2.subject)
	println("books2 bookid :", books2.bookid)
	println()
	printBook(books1)
	printBook(books2)

	// 结构体指针
	//你可以定义指向结构体的指针类似于其他指针变量，格式如下：
	var book5 *Books
	book5 = &books1

	println(book5)

}

func printBook(book Books) {
	fmt.Printf("Book title : %s\n", book.title)
	fmt.Printf("Book author : %s\n", book.author)
	fmt.Printf("Book subject : %s\n", book.subject)
	fmt.Printf("Book book_id : %d\n", book.bookid)
}
