package main

import "fmt"

// Go 语言通过内置的错误接口提供了非常简单的错误处理机制。
//
//error类型是一个接口类型，这是它的定义：

/*
type error interface {
		Error() string
}
*/

// 我们可以在编码中通过实现 error 接口类型来生成错误信息。

// 定义一个 DivideError 结构
type DivideError struct {
	// 除数
	dividee int
	// 被除数
	divider int
}

// 实现 `error` 接口
func (de DivideError) Error() string {
	strFormat := "    Cannot proceed, the divider is zero.\n    dividee: %d\n    divider: 0"
	return fmt.Sprintf(strFormat, de.dividee)
}

// 定义 `int` 类型除法运算的函数
func Divide(varDividee int, varDivider int) (result int, errorMsg string) {
	if varDivider == 0 {
		data := DivideError{
			dividee: varDividee,
			divider: varDivider,
		}
		errorMsg = data.Error()
		return
	} else {
		return varDividee / varDivider, ""
	}

}

func main() {
	// 正常情况
	if result, errorMsg := Divide(100, 10); errorMsg == "" {
		fmt.Printf("100/10 = ", result)
	}

	// 当除数为零的时候会返回错误信息
	if _, errorMsg := Divide(100, 0); errorMsg != "" {
		fmt.Println("errorMsg is: ", errorMsg)
	}
}
