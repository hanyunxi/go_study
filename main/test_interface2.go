package main

// 定义 接口
type Person interface {
	say()
}

type Men struct {
}

func (men Men) say() {
	println("我是男滴哦.....")
}

type Women struct {
}

func (women Women) say() {
	println("我是女滴哦....")
}

func main() {
	var person Person
	person = new(Men)
	person.say()

	person = new(Women)
	person.say()
}
