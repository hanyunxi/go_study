package main

import (
	"fmt"
	"os"
)

// Go 语言中 range 关键字用于 for 循环中迭代数组(array)、切片(slice)、通道(channel)或集合(map)的元素。
//在数组和切片中它返回元素的索引和索引对应的值，在集合中返回 key-value 对。

func main() {
	var num = []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}

	for index, val := range num {
		fmt.Println("index: ", index, "value: ", val)
	}

	// for 循环的 range 格式可以省略 key 和 value，如下实例：
	map1 := make(map[int]float32)
	map1[1] = 1
	map1[2] = 2
	map1[3] = 3
	map1[4] = 4

	// 读取 key 和 value
	for key, value := range map1 {
		fmt.Println("key: ", key, "value", value)
	}
	// 读取 key
	for key := range map1 {
		fmt.Println("key:", key)
	}
	// 读取value
	for _, value := range map1 {
		fmt.Println("value: ", value)
	}

	// 数字累加
	nums := []int{1, 2, 3, 4, 5}
	sum := 0
	for index := range nums {
		sum += nums[index]
	}
	fmt.Println("sum: ", sum)
	//在数组上使用 range 将传入索引和值两个变量。上面那个例子我们不需要使用该元素的序号，所以我们使用空白符"_"省略了。有时侯我们确实需要知道它的索引。
	for index := range nums {
		if index == 3 {
			fmt.Println("index:", index)
		}
	}
	//range 也可以用在 map 的键值对上。
	fuirts := map[string]string{"a": "apple", "b": "banana"}
	for key, val := range fuirts {
		fmt.Printf("key:%s, val: %s", key, val)
		println()
	}

	// 通过 range 获取参数列表:
	for i, arg := range os.Args {
		fmt.Println("key: ", i, "val:", arg)
	}

}
