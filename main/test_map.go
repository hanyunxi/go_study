package main

import "fmt"

// Go 语言Map(集合)
//	Map 是一种无序的键值对的集合。Map 最重要的一点是通过 key 来快速检索数据，key 类似于索引，指向数据的值。

func main() {

	/*创建集合 */
	var counttryMap map[string]string = make(map[string]string)

	/*	var counttryMap  map[string]string
		counttryMap = make(map[string]string)*/

	counttryMap["France"] = "Paris"
	counttryMap["Italy"] = "Roman"
	counttryMap["Japan"] = "Tokyo"
	counttryMap["India"] = "新德里"

	for country, capital := range counttryMap {
		fmt.Println("country", country, "city: ", capital)
	}

	/*查看元素在集合中是否存在 */
	cap, ok := counttryMap["India"]

	if ok {
		fmt.Printf("captial: ", cap)
	} else {
		fmt.Println("captial: ", cap)
	}
	println()
	// delete() 函数用于删除集合的元素, 参数为 map 和其对应的 key。实例如下：
	delete(counttryMap, "India")
	captial, ok := counttryMap["India"]
	if ok {
		fmt.Println("India: ", captial)
	} else {
		fmt.Println("key: India 不存在！")
	}

}
