package main

import "fmt"

// Go 语言递归函数
//递归，就是在运行的过程中调用自己。

// 阶乘1
func Factorial(n uint64) (result uint64) {
	if n > 0 {
		result = n * Factorial(n-1)
		return result
	}
	return 1
}

func factorial(n int) int {
	res := 0
	if n > 0 {
		res = n * factorial(n-1)
		return res
	}
	return 1
}

// 斐波那契数列
func fibonacci(n int) int {
	if n < 2 {
		return n
	}
	return fibonacci(n-1) + fibonacci(n-2)
}

func main() {
	// 阶乘
	n := 4
	println("阶乘 result: ", Factorial(4))
	println("阶乘 res: ", factorial(n))

	// 斐波那契数列
	for i := 0; i < 10; i++ {
		fmt.Printf("%d\t", fibonacci(i))
	}
}
