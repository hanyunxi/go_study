package main

import "fmt"

func main() {
	// Go 语言类型转换
	// 类型转换用于将一种数据类型的变量转换为另外一种类型的变量。Go 语言类型转换基本格式如下：
	var sum int = 9
	count := 5
	var mean float64

	mean = float64(sum) / float64(count)
	res := sum / count
	fmt.Printf("mean 的值为: %f\\n", mean)
	println()
	fmt.Printf("res 的值为: %f\\n", res)

}
